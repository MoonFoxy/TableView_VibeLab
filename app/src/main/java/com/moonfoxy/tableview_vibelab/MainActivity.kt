package com.moonfoxy.tableview_vibelab

import android.graphics.Color
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import androidx.activity.ComponentActivity
import kotlin.random.Random

class MainActivity : ComponentActivity() {
        private val random = Random.Default //kotlin.random
        private lateinit var imageView: ImageView
        private lateinit var button: Button
        private val images = listOf(
            R.drawable.air_stonks,
            R.drawable.cat_meme,
            R.drawable.debats,
            R.drawable.kotak_zheme,
            R.drawable.kronks,
            R.drawable.luntik,
            R.drawable.mirn,
            R.drawable.mr_proper,
            R.drawable.shitshmig,
            R.drawable.stonks,
            R.drawable.wireless_shower,
            R.drawable.oops,
        )

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)

            imageView = findViewById(R.id.imageView)
            button = findViewById(R.id.button)
            button.setOnClickListener {
                val id: Int = random.nextInt(12)
                val color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256))
                imageView.setBackgroundResource(images[id])
                button.setBackgroundColor(color)
            }
        }

}
